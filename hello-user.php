<?php
session_start();
 
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: php/login-page.php");
    exit;
}
?>
 
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles/main-styles.css">
</head>
<body>
    <h1>Привет <?php echo htmlspecialchars($_SESSION["name"]); ?></h1>
    <p>
        <a href="php/logout.php">Выйти</a>
    </p>
    <img src="img/my_img.gif">
</body>
</html>