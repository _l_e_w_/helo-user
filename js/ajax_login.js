$( document ).ready(function() {
    $('#submit_btn').on('click', function() {
        var login = $('#login').val();
        var password = $('#password').val();

        $('#login_error').html('');
        $('#password_error').html('');

        $.ajax({
            url:'../php/login.php',
            type:'post',
            data:{login:login,password:password},
            success:function(response){
                if(response == 0){
                    window.location = "../hello-user.php";
                }
                else if(response == 1){
                    $('#password_error').html('Не правильный пароль');
                }
                else {
                    $('#login_error').html('Такого пользователя не существует');
                }
            }
        });
    });
});