$( document ).ready(function() {
    $('#submit_btn').on('click', function() {
        var login = $('#login').val();
        var password = $('#password').val();
        var confirm_password =$('#confirm_password').val();
		var email = $('#email').val();
        var name = $('#name').val();

        $('#login_error').html('');
        $('#password_error').html('');
        $('#confirm_password_error').html('');
        $('#email_error').html('');
        $('#name_error').html('');

        $validation_check = true;

        if(login.length < 6) {
            $('#login_error').html('Логин должен содержать минимум 6 символов');
            $validation_check = false;
        }

        if(password == "") {
            $('#password_error').html('Это поле должно быть заполнино');
            $validation_check = false;
        }
        else if(password.length < 6) {
            $('#password_error').html('Пароль должен содержать минимум 6 символов');
            $validation_check = false;
        }
        else if(!(password.match(/[a-zA-Z]/) && password.match(/[0-9]/))) {
            $('#password_error').html('Пароль должен содержать только цифры английские буквы');
            $validation_check = false;
        }

        if(confirm_password != password) {
            $('#confirm_password_error').html('Пароли должны совподать');
            $validation_check = false;
        }

        const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(email == "") {
            $('#email_error').html('Это поле должно быть заполнино');
            $validation_check = false;
        }
        else if(!email.match(re)) {
            $('#email_error').html('Неправильный формат почты');
            $validation_check = false;
        }

        if(name.length < 2) {
            $('#name_error').html('Имя должно содржать минимум 2 буквы');
            $validation_check = false;
        }
        else if(!name.match(/^[a-zA-Zа-яА-ЯёЁ]+$/)) {
            $('#name_error').html('Имя должно содржать только буквы');
            $validation_check = false;
        }

        if($validation_check) {
            $.ajax({
                url:'../php/register.php',
                type:'post',
                data:{login:login,password:password,email:email,name:name},
                success:function(response){
                    if(response == 0){
                        window.location = "../hello-user.php";
                    }
                    else if(response == 1){
                        $('#login_error').html('Такой логин уже зарегестрирован');
                    }
                    else if(response == 2){
                        $('#email_error').html('Такой email уже зарегестрирован');
                    }
                    else {
                        alert('Ошибка')
                    }
                }
            });
        }
    });
});