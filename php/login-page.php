<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="../js/ajax_login.js"></script>
        <link rel="stylesheet" href="../styles/main-styles.css">
    </head>
    <body>
        <form class="form register" id="login_form">
            <label>Логин</label><br>
            <input class="input" type="text" id="login"><br> <label class="label error" id="login_error"></label><br>
            <label>Пароль</label><br>
            <input class="input" type="password" id="password"><br> <label class="label error" id="password_error"></label><br>
            <input class="input button" type="button" id="submit_btn" value="Войти">
        </form>

        <a href="register-page.php">Регистрация</a>
    </body>
</html>