<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script src="../js/ajax_register.js"></script>
        <link rel="stylesheet" href="../styles/main-styles.css">
    </head>
    <body>
        <form class="form register" id="register_form">
            <label>Логин</label><br>
            <input class="input" type="text" id="login"><br> <label class="label error" id="login_error"></label><br>
            <label>Пароль</label><br>
            <input class="input" type="password" id="password"><br> <label class="label error" id="password_error"></label><br>
            <label>Потверждение пароля</label><br>
            <input class="input" type="password" id="confirm_password"><br> <label class="label error" id="confirm_password_error"></label><br>
            <label>email</label><br>
            <input class="input" type="email" id="email"><br> <label class="label error" id="email_error"></label><br>
            <label>Имя</label><br>
            <input class="input" type="text" id="name"><br> <label class="label error" id="name_error"></label><br>
            <input class="input button" type="button" id="submit_btn" value="Отправить">
        </form>
        <a href="login-page.php">Вход</a>
    </body>
</html>
