<?php
include 'user.php';

if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && 
   strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    $user = new User;
    $user->login = $_POST["login"];
    $user->password = $_POST["password"];
    $user->email = $_POST["email"];
    $user->name = $_POST["name"];
    $result = $user->register();

    if($result == 0) {
        session_start();
                                    
        $_SESSION["loggedin"] = true;
        $_SESSION["name"] = $user->name;
    }

    echo $result;
    exit;
}
else {
    exit;
}

?>