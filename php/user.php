<?php

class User {
    var $login;
    var $password;
    var $email;
    var $name;

    function register() {
        $hash = password_hash($this->password, PASSWORD_DEFAULT);
        $data = array("login" => $this->login, "password" => $hash, "email" => $this->email, "name" => $this->name);
        $content = file_get_contents('../data/DB.json');
        $tempArray = json_decode($content, true);
        foreach($tempArray as $entrie) {
            foreach($entrie as $key => $value) {
                if($key == "login")
                {
                    if($value == $this->login)
                    {
                        return 1;
                    }
                }
                if($key == "email")
                {
                    if($value == $this->email)
                    {
                        return 2;
                    }
                }
            }
        }
        array_push($tempArray, $data);
        file_put_contents('../data/DB.json', json_encode($tempArray));
        return 0;
    }

    function login() {
        $content = file_get_contents('../data/DB.json');
        $tempArray = json_decode($content, true);
        $search = 0;
        foreach($tempArray as $entrie) {
            foreach($entrie as $key => $value) {
                if($search == 0) {
                    if($key == "login")
                    {
                        if($value == $this->login)
                        {
                            $search = 1;
                        }
                    }
                }
                else if($search == 1){
                    if($key == "password")
                    {
                        if(password_verify($this->password, $value))
                        {
                            $search = 2;
                        }
                        else {
                            return 1;
                        }
                    }
                }
                else if($search == 2) {
                    if($key == "email")
                    {
                        $this->email = $value;
                        $search = 3;
                    }
                }
                else {
                    if($key == "name")
                    {
                        $this->name = $value;
                        return 0;
                    }
                }
            }
        }
        return 2;
    }
    
}

?>